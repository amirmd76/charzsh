from setuptools import setup, find_packages

readme = "Charzsh"

setup(
    name='charzsh',
    version='0.0.6',
    description='Charzsh',
    long_description=readme,
    author='AmirMohammad Dehghan',
    author_email='amirmd76@gmail.com',
    url='https://gitlab.com/amirmd76/charzsh.git',
    license='MIT',
    packages=find_packages(),
    install_requires=[
        'python-dateutil',
    ],
    entry_points={
        'console_scripts': [
            'chzsh-bu = charzsh.backup:main',
            'chzsh-rmbu = charzsh.delete_backedup:main',
            'chzsh-eta = charzsh.eta:main',
            'chzsh-fix-io-errors = charzh.fix_io_errors:main',
            'k8s-cfg = charzsh.k8s_cfg:main',
        ],
    },
)
